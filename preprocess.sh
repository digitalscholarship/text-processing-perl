#!/bin/bash
# =======================================================================
# FUNCTION DECLARATIONS
# =======================================================================
usage() {
  echo ""
  echo "Usage: preprocess.sh -C [-s] [-d] [-t] [-n] idir odir"
  echo "       -C          Process the input directory based on its case (1 for many files, 2 for csv/tsv)"
  echo "       -s          Size for each chunk, default=2MB"
  echo "       -d          If Case=2 this sets the delimiter to use, default='\t'"
  echo "       -t          If Case=2 this sets the text field"
  echo "       -n          Number of cores to use, default=4"
  echo " "
  echo "Examples:"
  echo "For Case 1 (many text files)"
  echo "      ./preprocess.sh -C 1 -s 200MB -n 30 idir odir"
  echo "For Case 2 (one to several tsv/csv files)"
  echo "      ./preprocess.sh -C 2 -s 200MB -d '\t' -t 6 -n 30 idir odir"
  echo ""
  exit 1
}

fix_names() {
  # use grep -n and only replace those filenames ...
  fix=$(tr -cd '[[:alnum:]]._-' <<< $1)
  if [ "$1" != "$fix" ]; then
    mv $1 $fix
  fi
}

remove_newlines() {
  perl -p -i -e 's/[\r\n]/ /g' $1
  echo "" >> $1
}

export -f fix_names
export -f remove_newlines

# =======================================================================
# PARSE ARGS
# =======================================================================
chunksize="2MB"
ncores=4

delim="\t" #only if case 2
tfield="" #only if case 2


while getopts "hC:s:d:t:n:" opt; do
  case $opt in 
    h)
      echo "help: "
      usage
      ;;
    C)
      echo "case triggered $OPTARG" 
      case=$OPTARG
      ;;
    s)
      echo "size triggered $OPTARG" 
      chunksize=$OPTARG
      ;;
    d)
      echo "delim triggered $OPTARG" 
      delim=$OPTARG
      ;;
    t)
      echo "tfield triggered $OPTARG" 
      tfield=$OPTARG
      ;;
    n)
      echo "ncores triggered $OPTARG" 
      ncores=$OPTARG
      ;;
    \?)
      echo "invalid option $OPTARG"
      usage
      ;;
  esac
done

idir=${@:OPTIND:1}
odir=${@:OPTIND+1:1}

if [ -z "$idir" ] || [ -z "$odir" ] ; then
  echo "Error: need to specify input and output directory!"
  usage
fi

if [ ! -d "$idir" ] ; then
  echo "Error: input directory doesn't exist!"
  exit 1
fi

if [ $case -ne 1 ] && [ $case -ne 2 ] ; then
  echo "Error: case needs to be equal to 1 or 2"
  exit 1
fi

if [ $case -eq 2 ] && [ -z "$tfield" ] ; then 
  echo "Error: case = 2 but text field not specified"
  exit 1
fi

# =======================================================================
# MAIN
# =======================================================================

# if output directory exists ask user if they want to proceed
if [ -d "$odir" ]; then
  "The output directory exists, are you sure you want to overwrite it?"
  select yn in "Yes" "No"; do
    case $yn in
      Yes ) break;;
      No ) exit;;
    esac
  done
fi

echo "making $odir"
if [ -d $odir ]; then 
  rm -rf $odir
fi
mkdir $odir 

echo "moving to $idir"
cd $idir
touch temp.dat
rm *.dat


## =======================================================================
### CASE 1: many documents each in unique text file
if [ $case = 1 ]; then
  echo "CASE 1"

  #1. fix problem filenames
  #   fix filenames that may cause problems for commands
  #   specifically ' " ( and ) 
  #   this is slow and inefficient but works
  echo "checking if filenames need to be fixed"
  if [ $(ls | grep ".txt" | grep -q -e "(" -e ")" -e "'" -e '"') ]; then
    echo "------fixing filenames"
    ls | grep ".txt" | parallel --jobs $ncores fix_names :::
  fi


  #2. join all documents
  #   create a file containing all the filenames in order
  #   for each file replace all newlines with a space
  #   do this because we want newlines to be the document delimiter
  #   use cat to append all the files into one adding a newline between each one
  echo "combining files"
  ls | grep ".txt" >> info.dat
  ls | grep ".txt" | parallel --jobs $ncores remove_newlines :::
  ls | grep ".txt" | xargs cat >> combined.dat 

  #3. split into equal sized parts
  #   move the filenames and combined file to the output directory
  #   split the combined file into chunks of a certain memory size (want about 50 - 500 total files)
  mv combined.dat $odir
  mv info.dat $odir
  cd $odir
  echo "splitting into $chunksize chunks"
  split -C $chunksize combined.dat --additional-suffix=.txt
  rm combined.dat # delete reduntant file

fi

## =======================================================================
### CASE 2: one or many documents each csv/tsv form
if [ $case = 2 ]; then
  echo "CASE 2"


  # 1. Join all files if multiple
  echo "joining files if needed"
  #if [ $(ls | wc -l) -gt 1 ]; then
  cat * >> combined.dat 

  # 2. Seperate into info and text based on config
  echo "seperate text from info"
  if [ $delim = '\t' ]; then # tab is default delim for cut
    cut -f $tfield combined.dat > alltext.dat # get text field
    cut --complement -f $tfield combined.dat > info.dat # get everything else
  else 
    cut -d $delim -f $tfield combined.dat > alltext.dat 
    cut -d $delim --complement -f $tfield combined.dat > info.dat 
  fi
  rm combined.dat

  #3. split text into equal sized parts
  #   move info and text to output directory
  #   split the text file into equal sized chunks 
  mv alltext.dat $odir
  mv info.dat $odir
  cd $odir
  echo "splitting into $chunksize chunks"
  split -C $chunksize alltext.dat --additional-suffix=.txt
  rm alltext.dat

fi
