# Introduction

This repository contains the scripts necessary for cleaning and formating corpus 
efficiently over many cores.

Assumes a unix/linux environment with *GNU utils*, *bash*, *GNU parallel* and *perl5* installed.
In addition, the tree-tagger program needs to be installed and working to perform the 
lemmatization. Tree tagger installation instructions can be found on the website:
http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/.

The main cleaning script requires Text::Hunspell as a dependency. Text::Hunspell can be installed
with cpan and requires hunspell to be built on the system.

## Workflow

1. **preprocess.sh** convert to convenient form   

2. **main.sh**  Clean, spellcheck and lemmatize the corpus

       + For each text in the corpus 
               - convert to lower  
               - convert patterns to common tags (emails, urls, phonenumbers, hashtags..)  
               - remove all words that don't appear in the dictionary list  (optional)
               - remove all punctuation symbols except for apostrophes and dashes within words  
               - remove all words that appear in stopwords list 
               - remove all single letters that aren't connected to any words  
               - pass the document to tree-tagger and keep the lemmatized versions of the words  
                tree-tagger returns <unknown> if there is no lemmatized version, in that  
                    case we simply keep the original non lemmatized word  


3. **cleanup.sh** combine into one file and remove empty documents 

## Todo
    
+ make the regex's for the 'common tags' much more robust
+ enable customization of the cleaning process through configuration files
+ add ways to keep track of some metrics of the cleaning process (words deleted ...)
+ measure performance
+ add support for different lemmatizers (morphadorner...)

## Contact

avkoehl at ucdavis dot edu
