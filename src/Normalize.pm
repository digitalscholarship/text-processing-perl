#!/usr/bin/perl

package Normalize;
use Exporter;
@ISA = ('Exporter');
@EXPORT = qw (
user hashtags common_tags remove_numbers remove_punctuation 
remove_singles trim_whitespace tokenize
);



## TWEETS
sub hashtags {
  $_ =~ s/(?:\#+[\w_]+[\w\'_\-]*[\w_]+)/HASHTAG$&/g;
  $_ =~ s/\#//g;
  return $_;
}
sub user {
  $_ =~ s/(?:\@+[\w_]+[\w\'_\-]*[\w_]+)/USER$&/g;
  $_ =~ s/\@//g;
  return $_;
}

## MAIN
my %operations = (
  '^"rt\s' => "RETWEET ",
  '^rt\s' => "RETWEET ",
  '\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))' => "WEBSITE",
  '\b[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\b' => "EMAIL", 
  '((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}' => "PHONENUMBER", 
  '\$[0-9]+\.[0-9][0-9]\b' => "PRICE", 
  '(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d' => "DATE",
  '\b([0-1]?[0-9]|[2][0-3]):([0-5][0-9])(:[0-5][0-9])?\b' => "TIME",
);

sub common_tags {
  for my $key (keys %operations) #perform all the substitutions
  {
    $_ =~ s/$key/$operations{$key}/g;
  }
  return $_;
}

sub remove_numbers {
  $_ =~ s/\d//g;
  return $_;
}

sub remove_punctuation {
  $_ =~ s/(?<=[A-Za-z])'(?=[A-Za-z])/APOSTROPHE/g;
  $_ =~ s/(?<=[A-Za-z])-(?=[A-Za-z])/HYPHEN/g;

  $_ =~ s/[[:punct:]]//g;

  $_ =~ s/APOSTROPHE/'/g;
  $_ =~ s/HYPHEN/-/g;
  return $_;
}

sub remove_singles {
  $_ =~ s/\s[a-z]\s/ /g;
  return $_;
}

sub trim_whitespace {
  #trim extra spaces
  $_ =~ s/\s+/ /g;

  $_ =~ s/^\s+//; # beginning of line
  $_ =~ s/\s+$//;# end of line
  return $_;
}

sub tokenize {
  $_ =~ s/([]\[\:\"\?\.\,\;\*\&\(\)])/ $1 /g;   # tokenize punctuation

  # note that this will remove all newlines
  $_ =~ s/\s+/ /g; # one space only between words
  return $_;
}


1;
