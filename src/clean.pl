# ====================================================================
#                          clean.pl
#     will read either from file specified on command line or stdin
#     cleans and tokenizes the file.
#
#     Flags 
#      -s : if stopwords should be stripped 
#      -d : if non dictionary words should be stripped
#      -t : if the corpus contains tweets 
#      -a : if triggered then adds all stopwords and 
#      		      dic files from ../include/additional/
# ====================================================================

#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Text::Hunspell;
use Normalize; 

my %options;
getopts("sdta", \%options);


## load stopwords
my %stopwords;
if ($options{s}) {
  my $stopfilepath = "../include/stopwords/stopwords.txt";
  open (my $stopfile, '<', $stopfilepath) or die $!;
  while (<$stopfile>){
    chomp;
    $stopwords{$_} = 1;
  }
}

## load dictionaries
my $speller = Text::Hunspell->new(
  "../include/dictionaries/en_US.aff",
  "../include/dictionaries/en_US.dic"
);
$speller->add_dic("../include/dictionaries/emoji.dic");
$speller->add_dic("../include/dictionaries/tags.dic");

## load emoji
my %emojis;
if ($options{t}) {
  open (my $emojifile, '<', "../include/emoji.csv") or die $1;
  while (<$emojifile>) {
    chomp;
    my @elem = split ',', $_;
    my $codepoint = $elem[0];
    my $desc = $elem[2];
    $desc =~ s/\s//g;
    $desc =~ s/[[:punct:]]//g;
    $emojis{$codepoint} = $desc;
  }
} # if tweets load in emoji dictionary 

### UNTESTED
## load additional
if ($options{a}) {
  if ($options{s}) {
    # load all stopwords (.txt) files:
    my @stopfiles = <../include/extra/*.txt>;
    foreach my $filename (@stopfiles) {
      open (my $file, '<', $filename);
      while (<$file>){
	chomp;
	$stopwords{$_} = 1;
      }
    } # foreach file
  }# if stopwords

  if ( $options{d}) {
    # load all dic files:
    my @dictfiles = <../include/extra/*.dic>;
    foreach my $dfilename (@dictfiles) {
      $speller->add_dic($dfilename);
    }
  }# if dictionary
}# if add
# ====================================================================
#                           MAIN
# ====================================================================
while( <> ) 
{
  ## clean -------------------------
  # always
  chomp;
  $_ = lc $_; 
  $_ = $_ . " DOCB";

  if (!$options{t}) {
    $_ =~ s/[^[:print:]]//g; # gets rid of emojis...
  }

  # tweets
  if ($options{t}) {
    $_ = save_emojis($_);
    $_ = hashtags($_);
    $_ = user($_);
  }# if tweets

  # default
  $_ = common_tags($_);
  $_ = remove_numbers($_);
  $_ = remove_singles($_);
  $_ = remove_punctuation($_);
  $_ = tokenize($_); # needs to come after common_tags

  my @words = split ' ', $_; 
  foreach my $word (@words) {
    chomp $word;

    # if word has uppercase (then its a tag or emoji...)
    if ($word =~ m/[A-Z]/) {
      print "$word\n";
      next;
    }

    if ($options{s} and $stopwords{$word}) {
      next;
    }

    # if word is original
    if ($options{d} and !$speller->check($word)) {
      next;
    }# if -d

    print "$word\n";
  } #for each word

} # for each line in infile


# ====================================================================
#                           FUNCTIONS
# ====================================================================
#
sub save_emojis {
  # add a space around each unicode grapheme and load into array
  my $temp = $_;
  $temp =~ s/(\X)/ $1 /g;
  my @elem = split ' ', $temp;

  # for each grapheme
  for my $w (@elem) {
    my $query = "";

    # split into individual characters
    my @chars = split "", $w;
    for my $c (@chars) {
      my $cp = ord($c);
      my $hex = sprintf("%X", $cp);
      my $newcp = "U+".$hex;
      if ($query eq "") {
	$query = $newcp;
      } else {
	$query = $query." ".$newcp;
      }
    }

    if ($emojis{$query}) {
      $_ =~ s/$w/ EMOJI$emojis{$query} /g;
    }
  }#foreach grapheme
  return $_;
}

