#!/usr/bin/perl
use strict;
use warnings;

# ====================================================================
#                           MAIN
#     - $ARGV0: full/path/file.txt
#     - $ARGV1: bow_dir
# ====================================================================
my @path = split '/', $ARGV[0];
my $base = $path[$#path];

open (my $infile, "<", $ARGV[0]) or die $!;
open (my $bowfile, '>', "$ARGV[1]/$base") or die $!;

sub create_bow {
  my @words = split ' ', $_; #split by whitespace
  my %bow;
  foreach my $w (@words) {
    $bow{$w} ++;
  }
  my $res = "";
  foreach my $w (keys %bow) {
    $res = $res.' '.$w.' '.$bow{$w};
  }
  return $res;
}


while( <$infile> ) 
{
  $_ = create_bow($_);
  print $bowfile "$_\n";
}

close($infile);
close($bowfile);
