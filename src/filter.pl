#!/usr/bin/perl
use strict;
use warnings;

# ====================================================================
#                           MAIN
#     - $ARGV0: /full/path/file.dat   
#            file to delete lines from
#     - $ARGV1: /full/path/indexs.dat 
#            file containing list of line numbers to delete
#     - $ARGV2: /full/path/output.dat  
# ====================================================================

open (my $infile, "<", $ARGV[0]) or die $!;
open (my $indexfile, "<", $ARGV[1]) or die $!;
open (my $ofile, ">", $ARGV[2]) or die $!;

chomp(my @k = <$indexfile>);
my %idxs = map { $_ => 1 } @k;

my $lc = 1;
while( <$infile> ) 
{
  if (!$idxs{$lc}) {
    print $ofile "$_";
  }

  $lc = $lc + 1;
}

close($infile);
close($indexfile);
close($ofile);
