usage() {
  echo ""
  echo "Usage: cleanup.sh infofile idir odir"
  echo ""
  exit 1
}

if [ $# -lt 3 ]; then
  usage
fi

info=$1
idir=$2
odir=$3

echo $1 $2 $3


if [ -d "$odir" ]; then
  echo "The output directory exists, are you sure you want to overwrite it?"
  select yn in "Yes" "No"; do
    case $yn in
      Yes ) break;;
      No ) exit;;
    esac
  done
fi
rm -rf $odir
mkdir $odir


cd $idir
cat * > corpus.dat
mv corpus.dat $odir
cp $info $odir
cd $odir


cd $odir
if [ "$(wc -l < corpus.dat)" != "$(wc -l < info.dat)" ]; then
  echo "mismatch in file lengths!"
  echo "$(wc -l corpus.dat)"
  echo "$(wc -l info.dat)"
  exit
fi
grep -e '^$' -n corpus.dat | sed -e 's/://g;' >> indexs.dat
if [ "$(wc -l < indexs.dat)" = 0 ]; then
  echo "no empty docs to filter out"
  rm indexs.dat
  exit
fi

cd $src 
echo "filtering empty documents"
perl filter.pl $odir/corpus.dat $odir/indexs.dat $odir/fixed_corpus.dat
perl filter.pl $odir/info.dat $odir/indexs.dat $odir/fixed_info.dat

rm $odir/corpus.dat $odir/info.dat
