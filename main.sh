#!/usr/bin/bash
ncores=30

lemma () {
  /opt/tree-tagger/cmd/tree-tagger-english  | awk '{if ($3 == "<unknown>") {print $1} else {print $3}}' 
}

cleanup () {
  perl -p -i -e 's/\n/ /g' $1
  perl -p -i -e 's/DOCB /\n/g' $1
  perl -p -i -e 's/DOCB//g' $1

  #all tags? emoji and so on...
  # e.g USERusername -> @username
}
usage() {
  echo ""
  echo "Usage: main.sh [-d] [-l] [-t] [-n] idir odir"
  echo "       -d          bool Determines if non dictionary words should be filtered out"
  echo "       -l          bool Deteriness if the corpus should be lemmatized"
  echo "       -t          bool Determiens if tweet specific cleaning should be done "
  echo "       -n          Number of cores to use, default=4"
  echo " "
  echo "Examples:"
  echo "Normal, spellcheck and lemma"
  echo "      ./main.sh -dl -n 30 idir odir"
  echo "Tweets, spellcheck and lemma"
  echo "      ./main.sh -dl -t -n 30 idir odir"
  exit 1
}

export -f lemma
export -f cleanup

ncores=4
while getopts "hdltn:" opt; do
  case $opt in
    h)
      usage
      ;;
    d)
      spell=true
      ;;
    l)
      lemma=true
      ;;
    t)
      tweets=true
      ;;
    n)
      ncores=$OPTARG
      ;;
    \?)
      echo "invalid option $OPTARG"
      usage
      ;;
  esac
done

idir=${@:$OPTIND:1}
odir=${@:$OPTIND+1:1}

if [ -z "$idir" ] || [ -z "$odir" ] ; then
   echo "need to specify input and output dir"
   usage
fi

if [ ! -d "$idir" ] ; then
  echo "input directory doesn't exist"
  exit 1
fi

if [ -d "$odir" ]; then
  echo "The output directory exists, are you sure you want to overwrite it?"
  select yn in "Yes" "No"; do
    case $yn in
      Yes ) break;;
      No ) exit;;
    esac
  done
  echo "deleting $odir"
  rm -r $odir
fi

echo "making $odir"
mkdir $odir

cd src

if [ "$spell" = true ] && [ "$lemma" = true ] ; then
  echo "cleaning, spellchecking and lemmatizing"
  if [ "$tweets" = true ] ; then
    ls $idir | grep ".txt" | parallel --jobs $ncores  "perl -C -I . clean.pl -tds $idir/{} | lemma > $odir/{}"
  else
    ls $idir | grep ".txt" | parallel --jobs $ncores  "perl -I . clean.pl -ds $idir/{} | lemma > $odir/{}"
  fi

elif [ "$spell" = true ] && [ "$lemma" != true ] ; then
  echo "cleaning and spellchecking"
  if [ "$tweets" = true ] ; then
    ls $idir | grep ".txt" | parallel --jobs $ncores  "perl -C -I . clean.pl -tds $idir/{}  > $odir/{}"
  else
    ls $idir | grep ".txt" | parallel --jobs $ncores  "perl -I . clean.pl -ds $idir/{} > $odir/{}"
  fi

elif [ "$lemma" = true ] && [ "$spell" != true ] ; then
  echo "cleaning and lemmatizing"
  if [ "$tweets" = true ] ; then
    ls $idir | grep ".txt" | parallel --jobs $ncores  "perl -C -I . clean.pl -ts $idir/{} | lemma > $odir/{}"
  else
    ls $idir | grep ".txt" | parallel --jobs $ncores  "perl -I . clean.pl -s $idir/{} | lemma > $odir/{}"
  fi

else
  echo "cleaning"
  if [ "$tweets" = true ] ; then
    ls $idir | grep ".txt" | parallel --jobs $ncores  "perl -C -I . clean.pl -ts $idir/{} > $odir/{}"
  else
    ls $idir | grep ".txt" | parallel --jobs $ncores  "perl -I . clean.pl -s $idir/{} > $odir/{}"
  fi
fi

## CLEANUP
echo "cleaning up"
cd $odir
ls $odir | grep '.txt' | parallel --jobs $ncores cleanup ::: 
